#include <iostream>
#include <fstream>

#include <future>
#include <mutex>
#include <condition_variable>

#include "StartMonitor.h"
#include "RailMonitor.h"
#include "Barrier.h"


std::mutex protect_cout;
StartMonitor* wait_to_start = nullptr;
RailMonitor* rails = nullptr;
Barrier* cycle = new Barrier();

int runTrain(const char id, const int routeLength, const int* route){
  int cycles = 0;
  std::string message;

  cycle->join();
  wait_to_start->add();

  for(int i = 0; i < routeLength-1; i += 1){
    while(!rails->aquire(route[i], route[i+1])){
      cycles += 1;

      protect_cout.lock();
      std::cout << "At time step " << std::to_string(cycles) << " train " << id << " got stuck at station " << std::to_string(route[i]) << ".\n";
      protect_cout.unlock();

      cycle->block();
    }

    cycles += 1;

    protect_cout.lock();
    std::cout << "At time step " << std::to_string(cycles) << " train " << id << " goes from station " << std::to_string(route[i]) << " to station " << std::to_string(route[i+1]) << ".\n";
    protect_cout.unlock();

    cycle->block();

    rails->release(route[i], route[i+1]);

  }

  cycle->leave();
  return cycles;
}

int main(int argc, char** argv){
  std::cout << "Welcome to train route simiulation.\n";

  if(argc != 2){
    std::cout << "Massive train wreak occured, try again.\n";
    return 0;
  }

  int numStations;
  int numTrains;
  int* routeLength;
  int** routes;
  int* times;

  /*-Obtain Simulation Data---------------------------------------------------*/
  std::ifstream inFile;
  inFile.open(argv[1]);

  inFile >> numTrains;
  inFile >> numStations;

  routes = new int*[numTrains];
  routeLength = new int[numTrains];
  times = new int[numTrains];

  for(int i = 0; i < numTrains; i += 1){
    inFile >> routeLength[i];
    routes[i] = new int[routeLength[i]];

    for(int j = 0; j < routeLength[i]; j += 1){
      inFile >> routes[i][j];
    }
  }

  inFile.close();


  /*-Test Input from File-----------------------------------------------------*/
  std::cout << "There are " << numStations << " stations with " << numTrains << " trains having routes:\n";
  for(int i = 0; i < numTrains; i += 1){
    std::cout << (char) (i + 65) << ": ";

    for(int j = 0; j < routeLength[i]; j += 1){
      std::cout << routes[i][j] << " ";
    }

    std::cout << std::endl;
  }

  /*-Setup Simulation---------------------------------------------------------*/
  wait_to_start = new StartMonitor(numTrains);
  rails = new RailMonitor(numStations);
  std::future<int> *trains = new std::future<int>[numTrains];

  //Run train on route
  for(int i = 0; i < numTrains; i += 1){
    trains[i] = std::async(runTrain, (char) (i + 65), routeLength[i], routes[i]);
  }

  /*-Run Simulation-----------------------------------------------------------*/
  std::cout << "\n" << "Simulation has started.\n";
  wait_to_start->release();
  for(int i = 0; i < numTrains; i += 1){
    trains[i].wait();
    times[i] = trains[i].get();
  }
  std::cout << "Simulation has finished.\n\n";

  /*-Simulation Results-------------------------------------------------------*/
  for(int i = 0; i < numTrains; i += 1){
    std::cout << "Train " << char(i + 65) << " completed its route at time step " << times[i] << ".\n";
  }

  /*-Some Cleanup-------------------------------------------------------------*/
  delete wait_to_start;
  delete rails;
  delete cycle;
  delete times;
  for(int i = 0; i < numTrains; i += 1){
    delete routes[i];
  }
  delete[] routes;

  /*-Program Finished---------------------------------------------------------*/
  return 0;
}
