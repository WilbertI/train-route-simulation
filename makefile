PROJECT = TrainRoutesSimulator

#Modify the following for your environment
CPP = g++ -g -fPIC
LINK = g++ -g -fPIC
C_FLAGS =
L_FLAGS = -lpthread

$(PROJECT): $(PROJECT).o StartMonitor.o Barrier.o RailMonitor.o
	$(LINK) $(PROJECT).o StartMonitor.o Barrier.o RailMonitor.o -o $(PROJECT) $(L_FLAGS)

$(PROJECT).o: $(PROJECT).cpp
	$(CPP) $(C_FLAGS) -c $(PROJECT).cpp

StartMonitor.o: StartMonitor.h StartMonitor.cpp
	$(CPP) $(C_FLAGS) -c StartMonitor.cpp

Barrier.o: Barrier.h Barrier.cpp
	$(CPP) $(C_FLAGS) -c Barrier.cpp

RailMonitor.o: RailMonitor.h RailMonitor.cpp
	$(CPP) $(C_FLAGS) -c RailMonitor.cpp

clean:
	rm *.o
	rm $(PROJECT)
