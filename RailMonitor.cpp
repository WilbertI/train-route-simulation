#include "RailMonitor.h"

RailMonitor::RailMonitor(int stations){
  nStations = stations;
  nRails = stations * stations;
  rails = new std::mutex[nRails];
}

RailMonitor::~RailMonitor(){
  delete rails;
}

bool RailMonitor::aquire(int src, int dst){
  int r = getRailID(src, dst);
  bool success = rails[r].try_lock();

  return success;
}

void RailMonitor::release(int src, int dst){
  int r = getRailID(src, dst);
  rails[r].unlock();
}

int RailMonitor::getRailID(int src, int dst){
  return (src < dst) ? (src * nStations + dst) : (dst * nStations + src);
}
