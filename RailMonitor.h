#ifndef RAIL_MONITOR
#define RAIL_MONITOR

#include <mutex>

class RailMonitor{
  private:
    int nStations;
    int nRails;

    //all possible point to point connections between stations
    //assumed there is a direct route (no station in between) between all stations
    std::mutex* rails;

    int getRailID(int src, int dst);

  public:
    RailMonitor(int stations);
    ~RailMonitor();
    bool aquire(int src, int dst);
    void release(int src, int dst);
};

#endif
