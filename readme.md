Simple program that simulates multiple trains running predefined routes through several stations. Routes are executed as threads so trains can run simultaneously.

Run Application:
Build this application.
Run with ./TrainRoutesSimulator <input file>

Assumptions:
- There is a direct route between all stations.
- There is room for only one train on a rail between stations at any given time.
- Any station can support an infinite number of trains.
