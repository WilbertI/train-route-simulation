#ifndef BARRIER
#define BARRIER

#include <mutex>
#include <condition_variable>

class Barrier{
  private:
    std::mutex lock;
    std::condition_variable wait;

    int expected;
    int current;

    void release();

  public:
    Barrier();
    void join();
    void block();
    void leave();
};

#endif
