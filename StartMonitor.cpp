#include "StartMonitor.h"

StartMonitor::StartMonitor(int i){
  go = false;
  expected = i;
  current = 0;
}

void StartMonitor::add(){
  std::unique_lock<std::mutex> ul(lock);
  current += 1;
  if(go && current == expected){
    wait.notify_all();
  }else{
    wait.wait(ul);
  }
}

void StartMonitor::release(){
  std::unique_lock<std::mutex> ul(lock);
  go = true;
  if(current == expected){
    wait.notify_all();
  }
}
