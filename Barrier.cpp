#include "Barrier.h"

Barrier::Barrier(){
  expected = 0;
  current = 0;
}

void Barrier::join(){
  std::unique_lock<std::mutex> ul(lock);
  expected += 1;
}

void Barrier::block(){
  std::unique_lock<std::mutex> ul(lock);
  current += 1;
  if(current == expected){
    release();
  }else{
    wait.wait(ul);
  }
}

void Barrier::leave(){
  std::unique_lock<std::mutex> ul(lock);
  expected -= 1;

  if(current == expected){
    release();
  }
}

void Barrier::release(){
  current = 0;
  wait.notify_all();
}
