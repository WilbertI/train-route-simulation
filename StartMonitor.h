#ifndef START_MONITOR
#define START_MONITOR

#include <mutex>
#include <condition_variable>

class StartMonitor{
  private:
    std::mutex lock;
    std::condition_variable wait;

    int expected;
    int current;
    bool go;

  public:
    StartMonitor(int expected);
    void add();
    void release();
};

#endif
